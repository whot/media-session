# translation of pipewire.master-tx.te.po to Telugu
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Krishna Babu K <kkrothap@redhat.com>, 2009, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: pipewire.master-tx.te\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pipewire/pipewire-media-"
"session/issues/new\n"
"POT-Creation-Date: 2021-10-20 10:03+1000\n"
"PO-Revision-Date: 2012-01-30 09:56+0000\n"
"Last-Translator: Krishna Babu K <kkrothap@redhat.com>\n"
"Language-Team: Telugu <en@li.org>\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"

#: src/alsa-monitor.c:662
msgid "Built-in Audio"
msgstr "అంతర్గత ఆడియో"

#: src/alsa-monitor.c:666
msgid "Modem"
msgstr "మోడెమ్"

#: src/alsa-monitor.c:675
msgid "Unknown device"
msgstr ""
